﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPitQuiz2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("{0}", Enumerable.Range(1, 1267).Sum(Quiz));
            Console.Read();
        }

        static int Quiz(int n)
        {
            Debug.Assert(n >= 0);

            if (n < 10) return n; // This is the secret sauce.

            var digits = n.DigitsWithoutZeros();
            var even = digits.Where(d => d.IsEven()).Sum();
            var odd = digits.Where(d => !d.IsEven()).Aggregate(1, (x, y) => x * y);

            return Quiz(Math.Abs(even - odd));
        }

    }

    static class Extensions
    {
        public static IEnumerable<int> DigitsWithoutZeros(this int n)
        {
            Debug.Assert(n > 0);
            return n.ToString().Where(d => d != '0').Select(d => int.Parse(d.ToString()));
        }

        public static bool IsEven(this int n)
        {
            return n % 2 == 0;
        }
    }
}
